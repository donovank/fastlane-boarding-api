FROM ruby:2.2.3

RUN groupadd -g 501 leantaas && \
          useradd -g leantaas -u 501 -d /opt/leantaas leantaas && \
    apt-get update -qq && apt-get install -y build-essential \
    # for postgres
    libpq-dev \
    # for nokogiri
    libxml2-dev libxslt1-dev \
    # for capybara-webkit
    libqt4-webkit libqt4-dev xvfb \
    python python-dev python-pip python-virtualenv \
    nodejs \
    # cleanup
    && rm -rf /var/lib/apt/lists/*



WORKDIR /opt/leantaas/boarding
COPY Gemfile* ./
RUN gem install bundler \
    && bundle install

COPY . .

EXPOSE 3000

RUN touch /opt/leantaas/boarding/log/production.log \
    && chown -R leantaas:leantaas /opt/leantaas

WORKDIR /opt/leantaas/boarding
CMD bundle exec puma -C config/puma.rb

USER leantaas